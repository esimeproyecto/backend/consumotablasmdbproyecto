const express = require('express');
const app = express();
const bodyParser = require('body-parser');
var cors = require('cors');
var mariadb = require('mariadb');
const port = process.env.PORT || 3100;
const databaseName = 'world';

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

const pool = mariadb.createPool({
    host: 'localhost',
    user: 'root',
    password: 'cheis1342',
    database: databaseName,
    connectionLimit: 5
});


app.post('/getData1', async function(req, res) {
    let jsonFinal = `{ "tables" : [ `;
    let tables = await getTables();
    let column = [];
    let count = 0;
    tables.forEach(element => {
        column[count] = element.tables;
        jsonFinal += `{ "table_name": "${element.tables}" , "columns" : "" } `;
        if (count != tables.length - 1) {
            jsonFinal += `,`
        }
        count++;
    });
    jsonFinal += `] }`
    jsonFinal = JSON.parse(jsonFinal);
    for (let i = 0; i < column.length; i++) {
        column[i] = await getColumns(column[i]);
        jsonFinal.tables[i].columns = column[i];
    }

    res.json(jsonFinal);
});


app.get('/getData2', async function(req, res) {
    let jsonFinal = `{ "tables" : [ `;
    let tables = await getTables();
    let column = [];
    let count = 0;
    tables.forEach(element => {
        column[count] = element.tables;
        jsonFinal += `{ "table_name": "${element.tables}" , "columns" : "" } `;
        if (count != tables.length - 1) {
            jsonFinal += `,`
        }
        count++;
    });
    jsonFinal += `] }`
    jsonFinal = JSON.parse(jsonFinal);
    for (let i = 0; i < column.length; i++) {
        column[i] = await getColumns(column[i]);
        jsonFinal.tables[i].columns = column[i];
    }

    res.json(jsonFinal);
});

async function getTables() {

    let conn;
    let row;

    try {
        conn = await pool.getConnection();
        row = await conn.query(`SELECT TABLE_NAME AS tables 
        FROM INFORMATION_SCHEMA.TABLES 
        WHERE TABLE_SCHEMA = '${databaseName}';`);
        conn.release();
        return (row);
    } catch (err) {
        conn.release();
        throw err;
    };

}

async function getColumns(table) {

    let conn;
    let row;
    let array = [];
    let count = 0;

    try {
        conn = await pool.getConnection();
        row = await conn.query(`show columns from ${table};`);
        conn.release();
        row.forEach(element => {
            array[count] = element;
            count++;
        });
        return (array);
    } catch (err) {
        conn.release();
        throw err;
    };

}

app.listen(port, function() {
    console.log('El sitio de APIs inició correctamente en el puerto: ', port);
});

jsonExample = '{  "tables" : [ { "table_name": "empleados", "columns" : ["nombre",  "apellido_p",  "apellido_m"] }, { "table_name" : "puestos", "colums" : ["gerente",  "enlace", "coordinador"] } ] }';